# -*- coding: utf-8 -*-
"""
Copyright 2023 Nasser Darwish
Institute of Science and Technology Austria
GNU AFFERO GENERAL PUBLIC LICENSE, Version 3
The python server for an NIS TCP/IP client
"""

from enum import Enum

class Status(Enum):
    Idle  = 0
    Ready = 1
    Busy  = 2
       
class Messages():
    
    ariaHead = "ARIA_"
    ariaTail = "_AIRA"

    complete = "OP_COMPLETE"

class SktStr(Messages):

    def startupMsg():
        return \
            Messages.ariaHead + \
            "starting Server"  + \
            Messages.ariaTail
    
def main():
    print(SktStr.startupMsg())

main()