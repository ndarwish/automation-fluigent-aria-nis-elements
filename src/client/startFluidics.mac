// Copyright 2023 Nasser Darwish
// Institute of Science and Technology Austria
// GNU AFFERO GENERAL PUBLIC LICENSE, Version 3

int main(){
  
   
   if (setGlobals() == 0){


      // Load accessory macros
      sprintf(executionBuffer, "%s\NkSocket.mac","macroPth");
      RunMacro(executionBuffer);   
      sprintf(executionBuffer, "%s\shapeWindow.mac","macroPth");
      RunMacro(executionBuffer);
      sprintf(executionBuffer, "%s\fluClient.mac","macroPth");
      RunMacro(executionBuffer);
      //sprintf(executionBuffer, "%s\commands.mac","macroPth");
      //RunMacro(executionBuffer);



      // Initialize GUI
      nis2py_showDialog();
      appendMessage(serverPth);          
      appendMessage(envPth);
      appendMessage(macroPth);      
      appendMessage(clientIP);

      fluClient();

      appendMessage("closing...");
      //Wait(2);
      //NkWindow_DestroyWindow(hDlg);
   }
}

int setGlobals(){
   // Protocol
   global char SKT_MSG_RECEIVED[256];
   global char SKT_SRV_RDY[256];
   global char SKT_KILL[256];
   global char SRV_END[256];   
   // Names
   global int HEAD;
   global int TAIL;
   global char header[10];
   global char tail[10];
   
   global char settingsFileName[256];
   global char commandsFileName[256];
   // Paths (from defaults.xml)
   global char serverPth[256];
   global char macroPth[256];
   global char envPth[256];
   // Timeouts (from defaults.xml)
   global int timeout_ms;
   global int servTimeout_ms;
   // Addresses (from defaults.xml)
   global char clientIP[15];
   global int  clientPort;
   // Window handles
   global long hNis;
   global long hDlg;
   global long hCtrl;  
   // Communication
   global longlong nis_server_socket;
   global char portOrSrv[256];
   global char server[256];
   global char client[256];
   global char msgFlag[16]; //err; val; msg; idl
   // Some strings
   global char srvStr[256];   
   global char msgStr[2048];
   global char executionBuffer[256];

   int defaultLoadSucceeded;
   int commandLoadSucceeded;
   
   HEAD = 1;
   TAIL = 0;

   SKT_MSG_RECEIVED = "socket_msg_recv";
   SKT_SRV_RDY      = "socket_srv_rdy";
   SKT_KILL         = "socket_stop";
   SRV_END          = "server_stopped";

   header  = "ARIA_";
   tail    = "_AIRA";

   settingsFileName = "C:\Program Files\NIS-Elements\Macros\AR1A\defaults.xml";   
   defaultLoadSucceeded = loadDefaults(settingsFileName);
   
   // Defaults for an injection experiment
   //commandLoadSucceeded = loadCommands();
   
   return defaultLoadSucceeded;
}

int wipeGlobals(){
   return 0;
}


int loadDefaults(char* settingsFileName){

   char xmlBuffer[4096];
   char keyStr[128];
   char delimiter[64] = "value";

   // Default settings
   if (ExistFile(settingsFileName)){
      ReadFileEx(settingsFileName,xmlBuffer,4096,1);
   
      // List of relevant keys
      
      serverPth  = getStringFromKey(xmlBuffer, "serverPath");
      envPth     = getStringFromKey(xmlBuffer, "envPath");      
      macroPth   = getStringFromKey(xmlBuffer, "macroPath");
      clientIP   = getStringFromKey(xmlBuffer, "clientIP");
      clientPort = getIntFromKey(xmlBuffer,    "clientPort");

      timeout_ms = getIntFromKey(xmlBuffer,    "timeoutms");
      servTimeout_ms = getIntFromKey(xmlBuffer,"serverTimeoutms");
      
      return 0;
   }
   else {
      return -1;
   }
}






int getIntFromKey(char* buffer, char* datakey){
   buffer = getStringFromKey(buffer, datakey);
   return atoi(buffer);
}
double getDoubleFromKey(char* buffer, char* datakey){
   buffer = getStringFromKey(buffer, datakey);
   return atof(buffer);
}







char* getStringFromKey(char* buffer, char* dataKey){
   // Returns the string from <datakey><value>string</value></datakey>
   int lenToCopy;   
   char bufferTail[strlen(buffer)];
   char tempBuffer[strlen(buffer)];
   char outputBuffer[strlen(buffer)];
   if ((buffer!= NULL) && (strlen(buffer) != 0)){   
      tempBuffer = splitByKey(buffer, dataKey, HEAD);
      bufferTail = splitByKey(tempBuffer, "value", HEAD);
      outputBuffer = splitByKey(bufferTail, "/value", TAIL);
   }
   return outputBuffer;
}


char* splitByKey(char* buffer, char* datakey, int headOrTail){
   // Chops off the head or the tail of a char string 
   // 'dataKey' is the slitting string,
   // buffer is the input
   // headOrTail is 1 for head (e.g. <value>
   // and 0 for tail (e.g._ACC_AutoCorrection </value>)
   int splitStrLen, chopLen;
   char tempBuffer[strlen(buffer)];
   char splitStr[256];

   sprintf(splitStr,"<%s>","datakey");
   splitStrLen = strlen(splitStr);

   if ((buffer!= NULL) && (strlen(buffer) != 0)){      
      if (headOrTail == 1){
         // headOrTail == 1 means head. We chop the head
         tempBuffer = strstr(buffer, splitStr) + splitStrLen;
      }else {
         // headOrTail == 0 means tail.  We chop the tail.
         tempBuffer = "";
         if (strstr(buffer, splitStr) != NULL){         
            chopLen = strlen(buffer) - strlen(strstr(buffer, splitStr));
            memcpy(tempBuffer, buffer, chopLen*2); //2 bytes per char. This was a tough one.
         }
      }
      return tempBuffer;
   }
   else {      
      return NULL;
   }
}
                        