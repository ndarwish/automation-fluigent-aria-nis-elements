Copy these files together into the NIS Macro path, for instance:

C:\Program Files\NIS-Elements\Macros\AR1A

Please note that the default path for macros is C:\Program Files\NIS-Elements\Macros. The only requirement is that the path matches what is written in defaults.xml
