# -*- coding: utf-8 -*-
"""
Created on Fri Jun 30 10:35:47 2023

@author: ndarwish
"""

from enum import Enum

class Status(Enum):
    Idle  = 0
    Ready = 1
    Busy  = 2
       
class Messages():
    
    ariaHead = "ARIA_"
    ariaTail = "_AIRA"

    complete = "OP_COMPLETE"

class SktStr(Messages):

    def startupMsg():
        return \
            Messages.ariaHead + \
            "starting Server"  + \
            Messages.ariaTail
    
def main():
    print(SktStr.startupMsg())

main()