The TCP server is a Python application. It is recommended to create an environment and a 'src' folder inside it for these files.

The client is a NIS macro (basically a C interpreter, but with no capabilities for dynamic memory allocation). The 'client' folder contains all macros, being 'startfluidics.mac' the entry point. This macro also starts the TCP server and before sending requests. These files are normally saved in the NIS macro folder: C:\Program files\NIS-Elements\Macros\AR1A. It is important to keep "defaults.xml" together with them.
In the defaults file there are some paths that can be readjusted. Currently, the server environment is expected to be in D:\AR1A, and the server program, ariaServer.py, is expected at D:\AR1A\src\

In the current stage, messages are sent correctly between NIS and the server, and there is a simple protocol where messages heve the following format:

requests: (NIS -> Aria) 
aria_ command argument# argument1 argument2 ... argumentN _aira

replies (aria -> NIS): 
aria_ error content _aira / aria_ message content _aira / aria_ value content _aira

These packages are assembled and parsed byt he server and the client by dedicated functions.

![Encryption](packages.jpg?raw=true "package structure")

The organization of the programs is the following:

![Encryption](scheme.jpg?raw=true "Software scheme")

The parsing of the requests is successful at the server and we are currently setting up a list of commands to execute

The next steps include the packing of the error messages for returning them to NIS and a menu selector on the NIS macro window for testing them interactively.

The macros depend on the following  packages: NKSocket.mac, NkWindow.mac and Win32.mac (together with their respective DLLs). It is currently possible to get permisssion from Nikon to get these files, we can put you in contact about it.

![Encryption](nis2py.jpg?raw=true "NIS app and Server windows")

